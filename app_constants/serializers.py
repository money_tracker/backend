from pydantic import BaseModel


class AppSettingCreateSerializer(BaseModel):
    name: str
    value: float = 0

    class Config:
        orm_mode = True


class AppSettingReadSerializer(BaseModel):
    id: int
    name: str
    value: float

    class Config:
        orm_mode = True


class AppSettingUpdateSerializer(BaseModel):
    value: float
