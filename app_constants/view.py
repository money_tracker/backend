from typing import List

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app_constants.crud import get_user_settings, update_user_setting
from app_constants.serializers import AppSettingReadSerializer, AppSettingUpdateSerializer
from db_settings import get_session
from security.security import get_current_user
from user.serializers import UserSerializer

router = APIRouter()


@router.get("/settings", tags=["settings"], response_model=List[AppSettingReadSerializer])
async def get_app_settings_view(session: AsyncSession = Depends(get_session),
                                current_user: UserSerializer = Depends(get_current_user)):
    user_app_settings = await get_user_settings(session, current_user.email)
    return user_app_settings


@router.put("/settings/{setting_id}", tags=["settings"], response_model=AppSettingReadSerializer)
async def update_settings(setting_id: int,
                          setting: AppSettingUpdateSerializer,
                          session: AsyncSession = Depends(get_session),
                          current_user: UserSerializer = Depends(get_current_user)):
    updated_user_setting = await update_user_setting(session, setting_id, setting, current_user.email)
    return updated_user_setting
