from typing import List

from fastapi import HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy.orm import selectinload
from starlette import status

from app_constants.models import AppSetting
from app_constants.serializers import AppSettingCreateSerializer, AppSettingUpdateSerializer
from user.models import User


async def get_app_setting(session: AsyncSession, setting_id: int):
    spent_item = await session.execute(select(AppSetting).where(AppSetting.id == setting_id))
    return spent_item.scalar()


async def create_init_app_settings(session: AsyncSession) -> List[AppSetting]:
    usd = AppSettingCreateSerializer(name='usd')
    euro = AppSettingCreateSerializer(name='euro')
    eurousd = AppSettingCreateSerializer(name='eurousd')

    created_app_settings = [AppSetting(**usd.dict()), AppSetting(**euro.dict()), AppSetting(**eurousd.dict())]

    session.add_all(created_app_settings)
    await session.flush()

    return created_app_settings


async def get_user_settings(session: AsyncSession, user_email) -> List[AppSetting]:
    user = await session.execute(
        select(User).where(User.email == user_email).options(selectinload(User.app_settings))
    )
    return user.scalar().app_settings


async def update_user_setting(session: AsyncSession, setting_id: int, setting: AppSettingUpdateSerializer,
                              user_email) -> AppSetting:
    existing_user_settings = await get_user_settings(session, user_email)
    existing_setting = await get_app_setting(session, setting_id)
    existing_user_settings_ids = [sett.id for sett in existing_user_settings]
    if existing_setting.id in existing_user_settings_ids:
        existing_setting.value = setting.value
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User doesn't have this setting",
        )
    await session.flush()
    return existing_setting
