from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app_constants.view import router as app_constants_router
from category.view import router as categories_router
from spent_item.view import router as spent_item_router
from user.view import router as users_router
from income.view import router as income_router

app = FastAPI()
app.include_router(spent_item_router)
app.include_router(categories_router)
app.include_router(app_constants_router)
app.include_router(users_router)
app.include_router(income_router)

origins = ["http://localhost:8091"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
