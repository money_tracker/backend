from calendar import monthrange
from datetime import date


async def limit_offset(limit: int = 10, offset: int = 0):
    return {"limit": limit, "offset": offset}


def get_first_and_last_month_dates(month_date: date):
    last_day = monthrange(month_date.year, month_date.month)[1]
    start_date = date(month_date.year, month_date.month, 1)
    end_date = date(month_date.year, month_date.month, last_day)
    return start_date, end_date
