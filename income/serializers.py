from typing import Optional

from pydantic import BaseModel


class CreateIncomeSerializer(BaseModel):
    salary: float
    additional_payments: Optional[float]


class ReadIncomeSerializer(BaseModel):
    id: int
    salary: Optional[float]
    additional_payments: Optional[float]

    class Config:
        orm_mode = True
