from datetime import date

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from income.models import Income
from income.serializers import CreateIncomeSerializer, ReadIncomeSerializer
from user.crud import get_user
from user.models import User
from utilities import get_first_and_last_month_dates


async def create_income(session: AsyncSession, user_email: str, income: CreateIncomeSerializer) -> Income:
    user = await get_user(session, user_email)
    income.salary = float(format(income.salary, ".2f"))
    income.additional_payments = float(format(income.additional_payments, ".2f"))
    existing_income = await get_income(session, user_email, date.today())
    if existing_income:
        existing_income.salary = income.salary
        existing_income.additional_payments = income.additional_payments
        await session.commit()
        return existing_income
    created_income = Income(**income.dict(), user=user)
    session.add(created_income)
    await session.flush()
    return created_income


async def get_income(session: AsyncSession, user_email: str, month_date: date) -> Income:
    start_date, end_date = get_first_and_last_month_dates(month_date)
    query = (
        select(Income)
        .join(User)
        .where(Income.create_on >= start_date, Income.create_on <= end_date, User.email == user_email)
    )
    result = await session.execute(query)
    return result.scalar()


async def get_total_income(session: AsyncSession, user_email: str) -> float:
    query = select(Income).join(User).where(User.email == user_email)
    result = await session.execute(query)
    incomes = [ReadIncomeSerializer.from_orm(rec) for rec in result.scalars().all()]
    total = sum([income.salary + income.additional_payments for income in incomes])
    return total
