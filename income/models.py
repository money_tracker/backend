from datetime import date

from sqlalchemy import Column, Integer, ForeignKey, Float, Date
from sqlalchemy.orm import relationship

from db_settings import Base


class Income(Base):
    __tablename__ = "incomes"

    id = Column(Integer, primary_key=True, index=True)
    salary = Column(Float)
    additional_payments = Column(Float)
    create_on = Column(Date, default=date.today())
    user_id = Column(Integer, ForeignKey("users.id"))

    user = relationship("User", back_populates="incomes")
