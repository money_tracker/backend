from datetime import date

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from db_settings import get_session
from income.crud import create_income, get_income
from income.serializers import CreateIncomeSerializer, ReadIncomeSerializer
from security.security import get_current_user
from user.serializers import UserSerializer

router = APIRouter()


@router.post("/incomes", status_code=status.HTTP_201_CREATED, response_model=ReadIncomeSerializer, tags=["income"])
async def create_income_view(income: CreateIncomeSerializer,
                             session: AsyncSession = Depends(get_session),
                             current_user: UserSerializer = Depends(get_current_user)):
    result = await create_income(session, current_user.email, income)
    return result


@router.get("/incomes", tags=["income"])
async def get_income_view(month_date: date,
                          session: AsyncSession = Depends(get_session),
                          current_user: UserSerializer = Depends(get_current_user)):
    result = await get_income(session, current_user.email, month_date)
    return result
