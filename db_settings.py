import os

from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import declarative_base, sessionmaker

DRIVER = "postgresql+asyncpg"
USER = os.getenv("POSTGRES_USER")
PASSWORD = os.getenv("POSTGRES_PASSWORD")
HOST = os.getenv("DB_HOST")
PORT = os.getenv("DB_PORT")
DB_NAME = os.getenv("POSTGRES_DB")

if os.getenv("ENV") == "prod":
    SQLALCHEMY_DATABASE_ASYNC_URL = f"{DRIVER}://{USER}:{PASSWORD}@{HOST}:{PORT}/{DB_NAME}"
else:
    SQLALCHEMY_DATABASE_ASYNC_URL = f"{DRIVER}://postgres:postgres@localhost:5432/money_tracker"

engine = create_async_engine(SQLALCHEMY_DATABASE_ASYNC_URL, echo=True)
async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)
Base = declarative_base()


async def get_session():
    async with async_session() as session:
        async with session.begin():
            yield session
