FROM python:3.11

RUN apt update \
    && apt install wget zsh -y
RUN sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
RUN curl -sSL https://install.python-poetry.org | python -
ENV PATH="${PATH}:/root/.local/bin"
RUN poetry config virtualenvs.create false
RUN poetry config virtualenvs.in-project false

RUN mkdir /app
WORKDIR /app
COPY . .

RUN poetry install --no-dev
