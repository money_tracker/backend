from typing import List

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from category.crud import get_user_categories, add_category_to_user, update_user_category, delete_user_category
from category.serializers import CategoryReadSerializer, CategoryCreateSerializer
from db_settings import get_session
from security.security import get_current_user
from user.serializers import UserSerializer

router = APIRouter()


@router.get("/categories", tags=["categories"], response_model=List[CategoryReadSerializer])
async def get_categories_view(session: AsyncSession = Depends(get_session),
                              current_user: UserSerializer = Depends(get_current_user)):
    categories = await get_user_categories(session, current_user.email)
    return categories


@router.post("/categories", tags=["categories"], response_model=CategoryReadSerializer,
             status_code=status.HTTP_201_CREATED)
async def add_category_view(category: CategoryCreateSerializer,
                            session: AsyncSession = Depends(get_session),
                            current_user: UserSerializer = Depends(get_current_user)):
    added_category = await add_category_to_user(session, category, current_user.email)
    return added_category


@router.put("/categories/{category_id}", tags=["categories"], response_model=CategoryReadSerializer)
async def change_category_view(category_id: int,
                               category: CategoryCreateSerializer,
                               session: AsyncSession = Depends(get_session),
                               current_user: UserSerializer = Depends(get_current_user)):
    updated_category = await update_user_category(session, category_id=category_id, category_name=category.category,
                                                  user_email=current_user.email)
    return updated_category


@router.delete("/categories/{category_id}", tags=["categories"], status_code=status.HTTP_204_NO_CONTENT)
async def delete_category_view(category_id: int,
                               session: AsyncSession = Depends(get_session),
                               current_user: UserSerializer = Depends(get_current_user)):
    await delete_user_category(session, category_id=category_id, user_email=current_user.email)
    return {"message": "Category deleted"}
