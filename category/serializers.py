from pydantic import BaseModel


class CategoryCreateSerializer(BaseModel):
    category: str


class CategoryReadSerializer(CategoryCreateSerializer):
    id: int

    class Config:
        orm_mode = True
