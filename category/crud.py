from typing import List

from fastapi import HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy.orm import selectinload
from starlette import status

from category.models import Category
from category.serializers import CategoryCreateSerializer
from user.models import User


async def create_init_categories(session: AsyncSession) -> List[Category]:
    food = CategoryCreateSerializer(category='food')
    cafe = CategoryCreateSerializer(category='cafe')
    health = CategoryCreateSerializer(category='health')
    fuel = CategoryCreateSerializer(category='fuel')

    init_categories = [Category(**food.dict()), Category(**cafe.dict()), Category(**health.dict()),
                       Category(**fuel.dict())]
    session.add_all(init_categories)
    await session.flush()
    return init_categories


async def get_user_categories(session: AsyncSession, user_email: str) -> List[Category]:
    user = await session.execute(
        select(User).where(User.email == user_email).options(selectinload(User.categories))
    )
    return user.scalar().categories


async def get_category(session: AsyncSession, category_id: int = None):
    data = await session.execute(select(Category).where(Category.id == category_id))
    return data.scalar()


async def create_category(session: AsyncSession, category: CategoryCreateSerializer) -> Category:
    category.category = category.category.lower()
    new_category = Category(**category.dict())
    session.add(new_category)
    await session.flush()
    return new_category


async def add_category_to_user(session: AsyncSession, category: CategoryCreateSerializer, user_email: str) -> Category:
    user_categories = await get_user_categories(session, user_email)
    user_categories_names = [category.category for category in user_categories]
    if category.category.lower() in user_categories_names:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="You already got this category",
        )
    else:
        new_category = await create_category(session, category)
        adding_category = new_category

    user_categories.append(adding_category)
    await session.flush()
    return adding_category


async def update_user_category(session: AsyncSession, category_id: int, category_name: str,
                               user_email: str) -> Category:
    existing_category = await get_category(session, category_id=category_id)
    user_categories = await get_user_categories(session, user_email)
    user_categories_ids = [category.id for category in user_categories]
    if existing_category.id in user_categories_ids:
        existing_category.category = category_name.lower()
        await session.commit()
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User doesn't have this category",
        )

    return existing_category


async def delete_user_category(session: AsyncSession, category_id: int, user_email: str):
    existing_category = await get_category(session, category_id=category_id)
    user_categories = await get_user_categories(session, user_email)
    user_categories_ids = [category.id for category in user_categories]
    if existing_category.id in user_categories_ids:
        user_categories.remove(existing_category)
        await session.commit()
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User doesn't have this category",
        )
