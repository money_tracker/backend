from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from db_settings import Base


class Category(Base):
    __tablename__ = "categories"

    id = Column(Integer, primary_key=True, index=True)
    category = Column(String)
    user_id = Column(Integer, ForeignKey("users.id"))

    spent_item = relationship("SpentItem", back_populates="category")
    user = relationship("User", back_populates="categories")
