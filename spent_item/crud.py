from datetime import date
from typing import List, Optional

from fastapi import HTTPException
from sqlalchemy import delete, desc
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy.orm import joinedload
from starlette import status

from category.crud import get_category
from category.models import Category
from spent_item.models import SpentItem
from spent_item.serializers import ReadSpentItemSerializer, CreateSpentItemSerializer, UpdateSpentItemSerializer
from user.crud import get_user
from user.models import User
from user.serializers import UserSerializer
from utilities import get_first_and_last_month_dates


async def get_spent_item(session: AsyncSession, spent_item_id: int) -> SpentItem:
    spent_item = await session.execute(
        select(SpentItem).join(Category).where(SpentItem.id == spent_item_id).options(joinedload(SpentItem.category))
    )
    return spent_item.scalar()


async def get_user_spent_items(session: AsyncSession, user_email: str,
                               start_date: Optional[date] = None,
                               end_date: Optional[date] = None,
                               filter_date: Optional[date] = None) -> List[ReadSpentItemSerializer]:
    if filter_date:
        query = (
            select(SpentItem).join(User)
            .where(SpentItem.create_on == filter_date, User.email == user_email)
            .options(joinedload(SpentItem.category))
        )
    elif start_date and end_date:
        query = (
            select(SpentItem).join(User)
            .where(SpentItem.create_on >= start_date, SpentItem.create_on <= end_date, User.email == user_email)
            .options(joinedload(SpentItem.category))
        )
    else:
        query = (
            select(SpentItem).join(User)
            .where(User.email == user_email)
            .options(joinedload(SpentItem.category))
        )
    query = query.order_by(desc(SpentItem.create_on))
    res = await session.execute(query)
    resp = [ReadSpentItemSerializer.from_orm(spent_item) for spent_item in res.scalars().all()]
    return resp


async def add_spent_item(session: AsyncSession, user: UserSerializer, item: CreateSpentItemSerializer) -> SpentItem:
    user = await get_user(session, user.email)
    spent_item = SpentItem(**item.dict(), user=user)
    session.add(spent_item)
    await session.flush()
    new_item = await get_spent_item(session, spent_item.id)
    return new_item


async def update_user_spent_item(session: AsyncSession, spent_item_id: int, user_email: str,
                                 spent_item: UpdateSpentItemSerializer) -> SpentItem:
    existing_spent_item = await get_spent_item(session, spent_item_id=spent_item_id)
    user_spent_items = await get_user_spent_items(session, user_email)
    user_user_spent_items_ids = [category.id for category in user_spent_items]
    if existing_spent_item.id in user_user_spent_items_ids:
        existing_spent_item.amount = spent_item.amount
        existing_spent_item.create_on = spent_item.create_on
        if spent_item.category_id is not None:
            category = await get_category(session, spent_item.category_id)
            existing_spent_item.category_id = spent_item.category_id
            existing_spent_item.category = category
        await session.commit()
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User doesn't have this category",
        )

    return existing_spent_item


async def delete_spent_item(session: AsyncSession, spent_item_id: int):
    await session.execute(delete(SpentItem).where(SpentItem.id == spent_item_id))
    await session.commit()


async def delete_user_spent_item(session: AsyncSession, spent_item_id: int, user_email: str):
    existing_spent_item = await get_spent_item(session, spent_item_id=spent_item_id)
    user_spent_items = await get_user_spent_items(session, user_email)
    user_categories_ids = [spent_item.id for spent_item in user_spent_items]
    if existing_spent_item.id in user_categories_ids:
        await delete_spent_item(session, spent_item_id)
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User doesn't have this category",
        )


async def get_total_consumption(session: AsyncSession, user_email: str, today: date = None) -> float:
    if today:
        start_date, end_date = get_first_and_last_month_dates(today)
        spent_items = await get_user_spent_items(session, user_email, start_date=start_date, end_date=end_date)
    else:
        spent_items = await get_user_spent_items(session, user_email)
    total = sum([spent_item.amount for spent_item in spent_items])
    return total
