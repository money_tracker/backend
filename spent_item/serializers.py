from datetime import date
from typing import Optional

from pydantic import BaseModel

from category.serializers import CategoryReadSerializer


class CreateSpentItemSerializer(BaseModel):
    amount: float
    category_id: int
    create_on: Optional[date]


class ReadSpentItemSerializer(BaseModel):
    id: int
    amount: float
    category: CategoryReadSerializer
    create_on: Optional[date]

    class Config:
        orm_mode = True


class UpdateSpentItemSerializer(BaseModel):
    amount: Optional[float]
    category_id: Optional[int]
    create_on: Optional[date]

    class Config:
        orm_mode = True


class ReadTotalCashSerializer(BaseModel):
    total_consumption: float
    total_incomes: float
    left: float
