from datetime import date
from typing import List, Optional

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from db_settings import get_session
from income.crud import get_total_income
from security.security import get_current_user
from spent_item.crud import (get_user_spent_items, add_spent_item, delete_user_spent_item, update_user_spent_item,
                             get_total_consumption)
from spent_item.serializers import (CreateSpentItemSerializer, ReadSpentItemSerializer, UpdateSpentItemSerializer,
                                    ReadTotalCashSerializer)
from user.serializers import UserSerializer
from utilities import get_first_and_last_month_dates

router = APIRouter()


@router.get("/spent-items", tags=["spent-item"], response_model=List[ReadSpentItemSerializer])
async def get_items(filter_date: Optional[date] = date(year=2022, month=5, day=1),
                    current_user: UserSerializer = Depends(get_current_user),
                    session: AsyncSession = Depends(get_session)):
    spent_items = await get_user_spent_items(session, current_user.email, filter_date=filter_date)
    return spent_items


@router.get("/spent-items/diagram", tags=["spent-item"], response_model=List[ReadSpentItemSerializer])
async def get_month_items(month_date: Optional[date] = date(year=2022, month=5, day=1),
                          current_user: UserSerializer = Depends(get_current_user),
                          session: AsyncSession = Depends(get_session)):
    start_date, end_date = get_first_and_last_month_dates(month_date)
    spent_items = await get_user_spent_items(session, current_user.email, start_date=start_date, end_date=end_date)
    return spent_items


@router.post("/spent-items", tags=["spent-item"], status_code=201, response_model=ReadSpentItemSerializer)
async def create_item(item: CreateSpentItemSerializer,
                      current_user: UserSerializer = Depends(get_current_user),
                      session: AsyncSession = Depends(get_session)):
    new_item = await add_spent_item(session, current_user, item)
    return new_item


@router.put("/spent-items/{spent_item_id}", tags=["spent-item"], response_model=ReadSpentItemSerializer)
async def update_item(spent_item_id: int,
                      spent_item: UpdateSpentItemSerializer,
                      session: AsyncSession = Depends(get_session),
                      current_user: UserSerializer = Depends(get_current_user)):
    update_spent_item = await update_user_spent_item(session, spent_item_id, current_user.email, spent_item)
    return update_spent_item


@router.delete("/spent-items/{spent_item_id}", tags=["spent-item"], status_code=status.HTTP_204_NO_CONTENT)
async def delete_item(spent_item_id: int,
                      session: AsyncSession = Depends(get_session),
                      current_user: UserSerializer = Depends(get_current_user)):
    await delete_user_spent_item(session, spent_item_id, current_user.email)
    return {"message": "Item deleted"}


@router.get("/spent-items/month-total", tags=["spent-item"], status_code=status.HTTP_200_OK)
async def get_month_consumption(today: date,
                                session: AsyncSession = Depends(get_session),
                                current_user: UserSerializer = Depends(get_current_user)):
    total = await get_total_consumption(session, current_user.email, today)
    return total


@router.get("/spent-items/left", tags=["spent-item"], response_model=ReadTotalCashSerializer,
            status_code=status.HTTP_200_OK)
async def get_total_consumption_view(session: AsyncSession = Depends(get_session),
                                     current_user: UserSerializer = Depends(get_current_user)):
    total_consumption = await get_total_consumption(session, current_user.email)
    total_incomes = await get_total_income(session, current_user.email)

    return {
        "total_consumption": total_consumption,
        "total_incomes": total_incomes,
        "left": total_incomes - total_consumption
    }
