from datetime import datetime

from sqlalchemy import Column, ForeignKey, Integer, Float, Date
from sqlalchemy.orm import relationship

from db_settings import Base


class SpentItem(Base):
    __tablename__ = "spent_items"

    id = Column(Integer, primary_key=True, index=True)
    amount = Column(Float)
    create_on = Column(Date, default=datetime.utcnow())
    update_on = Column(Date, default=datetime.utcnow())
    user_id = Column(Integer, ForeignKey("users.id"))
    category_id = Column(Integer, ForeignKey("categories.id"))

    user = relationship("User", back_populates="spent_items")
    category = relationship("Category", back_populates="spent_item")
