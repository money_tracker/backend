from typing import Tuple, Union

from sqlalchemy import update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from app_constants.crud import create_init_app_settings
from category.crud import create_init_categories
from user.models import User
from user.serializers import UserUpdateSerializer, UserSerializer, UserCreateSerializer


async def create_user(session: AsyncSession, creating_user: UserCreateSerializer) -> str:
    from security.security import create_hashed_password, create_access_token

    hashed_password = create_hashed_password(creating_user.password)
    creating_user.password = hashed_password

    init_categories = await create_init_categories(session)
    init_app_settings = await create_init_app_settings(session)
    user = User(**creating_user.dict(), categories=init_categories, app_settings=init_app_settings)
    session.add(user)
    await session.flush()

    access_token = create_access_token(data={"sub": creating_user.email})
    return access_token


async def get_user(session: AsyncSession, email: str) -> Union[User, Tuple]:
    obj = await session.execute(select(User).where(User.email == email))
    user = obj.scalar()
    return user


async def get_users(session: AsyncSession, limit: int, offset: int) -> User:
    obj = await session.execute(select(User).limit(limit).offset(offset))
    users = obj.scalars().all()
    return users


async def update_user(session: AsyncSession, updating_user: UserUpdateSerializer, current_user_email) -> UserSerializer:
    from security.security import create_hashed_password
    queue = update(User).where(User.email == current_user_email)

    if updating_user.first_name:
        queue = queue.values(first_name=updating_user.first_name)

    if updating_user.last_name:
        queue = queue.values(last_name=updating_user.last_name)

    if updating_user.password:
        new_password = create_hashed_password(updating_user.password)
        queue = queue.values(password=new_password)

    queue.execution_options(synchronize_session="fetch")
    await session.execute(queue)
    return updating_user
