from typing import Optional

from pydantic import BaseModel


class UserSerializer(BaseModel):
    email: str
    first_name: Optional[str] = None
    last_name: Optional[str] = None

    class Config:
        orm_mode = True


class UserCreateSerializer(UserSerializer):
    password: str


class UserUpdateSerializer(UserSerializer):
    password: Optional[str]

    class Config:
        orm_mode = True
