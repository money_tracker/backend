from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from db_settings import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    password = Column(String)
    first_name = Column(String)
    last_name = Column(String)

    spent_items = relationship("SpentItem", back_populates="user")
    categories = relationship("Category", back_populates="user")
    app_settings = relationship("AppSetting", back_populates="user")
    incomes = relationship("Income", back_populates="user")
