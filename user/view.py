from typing import List

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from db_settings import get_session
from security.security import create_access_token, get_current_user, get_authenticated_user
from security.serializers import Token
from user.crud import get_users, update_user, create_user
from user.serializers import UserSerializer, UserCreateSerializer, UserUpdateSerializer
from utilities import limit_offset

router = APIRouter()


@router.post("/login", response_model=Token, tags=["users"])
async def login(form_data: OAuth2PasswordRequestForm = Depends(), session: AsyncSession = Depends(get_session)):
    user = await get_authenticated_user(session, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = create_access_token(data={"sub": user.email})
    return {"access_token": access_token, "token_type": "bearer"}


@router.post("/signup", response_model=Token, status_code=status.HTTP_201_CREATED, tags=["users"])
async def create_user_view(user: UserCreateSerializer, session: AsyncSession = Depends(get_session)):
    access_token = await create_user(session, user)
    return {"access_token": access_token, "token_type": "bearer"}


@router.get("/users", response_model=List[UserSerializer], dependencies=[Depends(get_current_user)], tags=["users"])
async def get_users_view(common: dict = Depends(limit_offset), session: AsyncSession = Depends(get_session)):
    users = await get_users(session, **common)
    return users


@router.get("/users/me", response_model=UserSerializer, tags=["users"])
async def get_current_user_view(current_user: UserSerializer = Depends(get_current_user)):
    return current_user


@router.put("/users/me", response_model=UserSerializer, tags=["users"])
async def update_current_user_view(updating_data: UserUpdateSerializer,
                                   current_user: UserSerializer = Depends(get_current_user),
                                   session: AsyncSession = Depends(get_session)):
    updated_user = await update_user(session, updating_data, current_user.email)
    return updated_user
